<?php

use PHPUnit\Framework\TestCase;
use src\Task9;

class Task9Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testPositive(array $array, int $number, mixed $expected): void
    {
        $response = (new Task9())->main($array, $number);
        $this::assertSame($expected, $response);
    }

    public function positiveProvider()
    {
        return [
            'Good' => [[2, 7, 7, 1, 8, 2, 7, 8, 7], 16, [
                '0' => '2 + 7 + 7 = 16',
                '1' => '7 + 1 + 8 = 16', ]],
            'GoodTwo' => [[2, 7, 7, 1, 2, 2, 12, 2, 2], 16, [
                '0' => '2 + 7 + 7 = 16',
                '1' => '2 + 2 + 12 = 16',
                '2' => '2 + 12 + 2 = 16',
                '3' => '12 + 2 + 2 = 16', ]],
        ];
    }

    /**
     * @dataProvider negativeProvider
     */
    public function testNegative(array $array, int $number): void
    {
        $obj = new Task9();
        $this->expectException(InvalidArgumentException::class);
        $obj->main($array, $number);
    }

    public function negativeProvider(): array
    {
        return [
            'Short' => [[2, 7], 16],
            'Negative number' => [[4, 6, 25], -15],
            'Zero' => [[4, 6, 25], 0],
            'Negative number in array' => [[4, 6, -25], 15],
        ];
    }
}
