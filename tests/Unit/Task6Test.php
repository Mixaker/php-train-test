<?php

use PHPUnit\Framework\TestCase;
use src\Task6;

class Task6Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testPositive(int $year, int $lastYear, int $month, int $lastMonth, mixed $expected): void
    {
        $response = (new Task6())->main($year, $lastYear, $month, $lastMonth);
        $this::assertSame($expected, $response);
    }

    public function positiveProvider()
    {
        return [
            'Good' => [1980, 2000, 02, 04, 35],
            'Good Two' => [2000, 2009, 01, 07, 16],
            'Good Three' => [1980, 2021, 05, 10, 71],
        ];
    }

    /**
     * @dataProvider negativeProvider
     */
    public function testNegative(int $year, int $lastYear, int $month, int $lastMonth): void
    {
        $obj = new Task6();
        $this->expectException(InvalidArgumentException::class);
        $obj->main($year, $lastYear, $month, $lastMonth);
    }

    public function negativeProvider(): array
    {
        return [
            'Incorrect date' => [-1900, 1900, 02, 03],
        ];
    }
}
